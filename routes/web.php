<?php

Route::get('/home', function () {
    return view('welcome');
});

//Route::get('/test', 'TestController@index')
//    ->name('test');

//storage route
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('storage.list');
    })->name('storages');
    Route::get('storage/create', 'StorageController@create')
        ->name('createStorage');
    Route::get('/storage/{storage}', 'StorageController@getSingle')
        ->where('id', '[0-9]+')
        ->name('getSingleStorage');
});

Auth::routes();