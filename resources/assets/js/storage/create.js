import Main from '../components/storage/create/Main';

const app = new Vue({
    el: '#app',
    components: {
        Main
    },
    render: h => h(Main)
});