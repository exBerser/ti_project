import Main from '../components/storage/single/Main.vue';

const app = new Vue({
    el: '#app',
    components: {
        Main
    },
    render: h => h(Main)
});