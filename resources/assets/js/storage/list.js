import Main from '../components/storage/list/Main.vue';

const app = new Vue({
    el: '#app',
    components: {
        Main
    },
    render: h => h(Main)
});