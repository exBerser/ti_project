<header class="header  push-down-45">
    <div class="container">
        <div class="logo  pull-left">
            <a href="{{route('storages')}}" id="logo-ti">
                TI
            </a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#readable-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <nav class="navbar  navbar-default" role="navigation">
            <div class="collapse  navbar-collapse" id="readable-navbar-collapse">
                <ul class="navigation">
                    {{--storage--}}
                    <li class="dropdown  active">
                        <a href="index.html" class="dropdown-toggle" data-toggle="dropdown">Storage</a>
                        <ul class="navigation__dropdown">
                            <li><a href="{{route('storages')}}">Storages</a></li>
                            <li><a href="{{route('createStorage')}}">Create new</a></li>
                            <li><a href="">Search</a></li>
                        </ul>
                    </li>
                    {{--tests--}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">test</a>
                        <ul class="navigation__dropdown">
                            <li><a href="#">All</a></li>
                            <li><a href="#">Create</a></li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="about-us.html" class="dropdown-toggle" data-toggle="dropdown">About</a>
                    </li>
                    <li class="">
                        <a href="contact.html" class="dropdown-toggle" data-toggle="dropdown">Contact with me</a>
                    </li>
                </ul>
            </div>
        </nav>

        {{--<div class="logout">--}}
            {{--<a href="{{ route('logout') }}"--}}
               {{--onclick="event.preventDefault();--}}
                   {{--document.getElementById('logout-form').submit();">--}}
                {{--{{ __('Logout') }}--}}
            {{--</a>--}}
            {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                {{--@csrf--}}
            {{--</form>--}}
        {{--</div>--}}
    <!-- Right Side Of Navbar -->
        <ul class="navbar-nav auth-nav">
            <!-- Authentication Links -->
            @guest
                <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle user-name" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</header>