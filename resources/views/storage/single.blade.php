@extends('layouts.main')

@section('content')
    <div id="app"></div>

{{--<div class="row storage-list">--}}
    {{--<div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">--}}
        {{--<div class="post-content">--}}
            {{--<a-scene v-pre>--}}
                {{--<h1 id="storage-title">{!! $storage->title !!}</h1>--}}
                {{--<div id="storage-body">{!! $storage->body !!}</div>--}}
            {{--</a-scene>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
            {{--<div class="col-xs-12  col-sm-6">--}}
            {{--</div>--}}
            {{--<div class="col-xs-12  col-sm-6">--}}
                {{--<div class="social-icons">--}}
                    {{--<p class="social-icons__container" v-on:click="editStorage"> <span class="Edit" >Edit</span></p>--}}
                    {{--<p class="social-icons__container" v-on:click="submitData"> <span class="Update" >Update</span></p>--}}
                    {{--<a href="#" class="social-icons__container"> <span class="Delete">Delete</span></a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="form-group">--}}
            {{--<br>--}}
            {{--<label for="tag">Tags</label>--}}
            {{--<input type="text" class="form-control" id="storage-tag" name="tag" aria-describedby="tagHelp" placeholder="Enter tag">--}}
            {{--<small id="tagHelp" class="form-text text-muted">You can add new tags separated by a comma</small>--}}
        {{--</div>--}}

        {{--<div class="row">--}}
            {{--<div class="col-xs-12  col-sm-6">--}}
                {{--<div class="post-author">--}}
                    {{--<h6>Written By</h6>--}}
                    {{--<hr>--}}
                    {{--<img src="images/dummy/about-5.jpg" alt="Post author">--}}
                    {{--<h5>--}}
                        {{--<a href="#">Danielle Thatcher</a>--}}
                    {{--</h5>--}}
                    {{--<span class="post-author__text">Interface Designer, Poker Player and Imaginary Breakdancer</span>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--@if(true === isset($storage->tags[0]))--}}
                {{--<div class="col-xs-12  col-sm-6">--}}
                    {{--<div class="tags  widget-tags">--}}
                        {{--<h6>Tags</h6>--}}
                        {{--<hr>--}}

                        {{--@foreach($storage->tags as $tag)--}}
                            {{--<a href={{route('getAllStorage', ['name' => $tag->name])}} class="tags__link">{{$tag->name}}</a>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endif--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@endsection

@section('script')
    @section('script')
        <script src="{{ asset('js/storage/single.js') }}"></script>
    @endsection
@endsection