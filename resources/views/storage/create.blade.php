@extends('layouts.main')

@section('content')
    <div class="container">
        <div id="app"></div>
        @include('layouts.validationErrors')
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/storage/create.js') }}"></script>
@endsection