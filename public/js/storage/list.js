/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 58);
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(5)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 5:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(59);


/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_storage_list_Main_vue__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_storage_list_Main_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_storage_list_Main_vue__);


var app = new Vue({
    el: '#app',
    components: {
        Main: __WEBPACK_IMPORTED_MODULE_0__components_storage_list_Main_vue___default.a
    },
    render: function render(h) {
        return h(__WEBPACK_IMPORTED_MODULE_0__components_storage_list_Main_vue___default.a);
    }
});

/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(61)
/* template */
var __vue_template__ = __webpack_require__(67)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/storage/list/Main.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-507dda0f", Component.options)
  } else {
    hotAPI.reload("data-v-507dda0f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__List_vue__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__List_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__List_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

function Storage(_ref) {
    var id = _ref.id,
        title = _ref.title,
        body = _ref.body,
        user = _ref.user;

    this.id = id;
    this.title = title;
    this.body = body;
    this.user = user;
}



/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            storages: [],
            currentUser: window.user[0]
        };
    },

    components: {
        StoragesList: __WEBPACK_IMPORTED_MODULE_0__List_vue___default.a
    },
    methods: {
        read: function read() {
            var _this = this;

            this.mute = true;
            window.axios.get('/api/storages').then(function (_ref2) {
                var data = _ref2.data;

                data.forEach(function (storage) {
                    _this.storages.push(new Storage(storage));
                });

                _this.mute = false;
            });
        },
        deleteStorage: function deleteStorage(id) {
            var _this2 = this;

            this.mute = true;

            window.axios.delete('/api/storages/' + id).then(function (_ref3) {
                var data = _ref3.data;

                if (data.remove) {
                    _this2.storages = _this2.storages.filter(function (storage) {
                        return storage.id != data.id;
                    });

                    $('#storage-' + data.id).remove(); // TODO: react deleted
                }

                _this2.mute = false;
            });
        }
    },
    created: function created() {
        this.read();
    }
});

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(63)
}
var normalizeComponent = __webpack_require__(1)
/* script */
var __vue_script__ = __webpack_require__(65)
/* template */
var __vue_template__ = __webpack_require__(66)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-bd98eb18"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/storage/list/List.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-bd98eb18", Component.options)
  } else {
    hotAPI.reload("data-v-bd98eb18", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(64);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("02463334", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd98eb18\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./List.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-bd98eb18\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./List.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 64:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n.action-menu[data-v-bd98eb18] {\n    float: right;\n}\nul[data-v-bd98eb18] {\n    font: 14px 'Verdana';\n    display: block;\n    margin: 0;\n    padding: 0;\n    list-style: none;\n    z-index: 5;\n}\nul[data-v-bd98eb18]:after {\n    display: block;\n    content: ' ';\n    clear: both;\n    float: none;\n}\nul.action-menu > li[data-v-bd98eb18] {\n    float: left;\n    position: relative;\n}\nul.action-menu > li > a[data-v-bd98eb18] {\n    display: block;\n    padding: 10px;\n    color: white;\n    background-color: rgba(14, 135, 67, 0.38);\n    text-decoration: none;\n}\nul.action-menu > li > a[data-v-bd98eb18]:hover {\n    background-color: #2a4c2b;\n}\nul.submenu[data-v-bd98eb18] {\n    display: none;\n    position: absolute;\n    top: 37px;\n    left: 0;\n    background-color: white;\n    border: 1px solid #539755;\n}\nul.submenu > li[data-v-bd98eb18] {\n    display: block;\n    cursor: pointer\n}\nul.submenu > li > a[data-v-bd98eb18] {\n    display: block;\n    padding: 10px;\n    color: white;\n    background-color: #5ca85e;\n    text-decoration: none;\n}\nul.submenu > li > a[data-v-bd98eb18]:hover {\n    text-decoration: none;\n    background: #6ac06c;\n    color: #fefefe;\n}\nul.action-menu > li:hover > ul.submenu[data-v-bd98eb18] {\n    display: block;\n}\n", ""]);

// exports


/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['id', 'title', 'body', 'user']
});

/***/ }),

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "boxed storage push-down-45",
      attrs: { id: "storage-" + _vm.id }
    },
    [
      _c("ul", { staticClass: "action-menu" }, [
        _c("li", [
          _c("a", { attrs: { href: "#" } }, [_vm._v("Action")]),
          _vm._v(" "),
          _c("ul", { staticClass: "submenu" }, [
            _c("li", [
              _c("a", { attrs: { href: "/storage/" + _vm.id } }, [
                _vm._v("Open")
              ])
            ]),
            _vm._v(" "),
            _c(
              "li",
              {
                on: {
                  click: function($event) {
                    _vm.$emit("delete-storage", _vm.id)
                  }
                }
              },
              [_c("a", [_vm._v("Delete")])]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-xs-10  col-xs-offset-1" }, [
          _c("div", { staticClass: "post-content--front-page" }, [
            _c("h2", { staticClass: "front-page-title" }, [
              _c("a", {
                attrs: { href: "/storage/" + _vm.id },
                domProps: { innerHTML: _vm._s(_vm.title) }
              })
            ]),
            _vm._v(" "),
            _c("div", {
              staticClass: "front-page-body",
              domProps: { innerHTML: _vm._s(_vm.body) }
            })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("div", { staticClass: "read-more" }, [
              _vm._v(
                "\n                        Create by " +
                  _vm._s(_vm.user.name) +
                  "\n\n\n                    "
              ),
              _c("div", { staticClass: "read-more__arrow" })
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-bd98eb18", module.exports)
  }
}

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container", attrs: { id: "app" } }, [
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xs-12  col-md-8 storages" },
        _vm._l(_vm.storages, function(storage) {
          return _c(
            "storages-list",
            _vm._b(
              { key: storage.id, on: { "delete-storage": _vm.deleteStorage } },
              "storages-list",
              storage,
              false
            )
          )
        })
      ),
      _vm._v(" "),
      _c("div", { staticClass: "col-xs-12  col-md-4" }, [
        _c("div", { staticClass: "widget-author  boxed  push-down-30" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-xs-10  col-xs-offset-1" }, [
              _c("h4", [_vm._v(_vm._s(_vm.currentUser.name))]),
              _vm._v(" "),
              _c("p", [_vm._v("...")])
            ])
          ])
        ]),
        _vm._v(" "),
        _vm._m(1)
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "widget-author__image-container" }, [
      _c("div", { staticClass: "widget-author__avatar--blurred" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "sidebar  boxed  push-down-30" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-xs-10  col-xs-offset-1" }, [
          _c("div", { staticClass: "widget-categories  push-down-30" }, [
            _c("h6", [_vm._v("Categories")]),
            _vm._v(" "),
            _c("ul", [
              _c("li", [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("Technology   "),
                  _c("span", { staticClass: "widget-categories__text" }, [
                    _vm._v("(16)")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("User Interface design   "),
                  _c("span", { staticClass: "widget-categories__text" }, [
                    _vm._v("(13)")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("User Experience   "),
                  _c("span", { staticClass: "widget-categories__text" }, [
                    _vm._v("(9)")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("Startups   "),
                  _c("span", { staticClass: "widget-categories__text" }, [
                    _vm._v("(23)")
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("Reading & Books   "),
                  _c("span", { staticClass: "widget-categories__text" }, [
                    _vm._v("(3)")
                  ])
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "widget-featured-post  push-down-30" }, [
            _c("h6", [_vm._v("Featured post")]),
            _vm._v(" "),
            _c("h4", [
              _c("a", { attrs: { href: "single-post.html" } }, [
                _vm._v("Featured Author Post")
              ])
            ]),
            _vm._v(" "),
            _c("p", [
              _vm._v(
                "Why ProteusThemes products is so much better than lorem everyone else's."
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "widget-posts  push-down-30" }, [
            _c("h6", [_vm._v("Popular / Recent")]),
            _vm._v(" "),
            _c("ul", { staticClass: "nav  nav-tabs" }, [
              _c("li", { staticClass: "active" }, [
                _c(
                  "a",
                  { attrs: { href: "#recent-posts", "data-toggle": "tab" } },
                  [
                    _c("span", { staticClass: "glyphicon  glyphicon-time" }),
                    _vm._v("  Recent Posts ")
                  ]
                )
              ]),
              _vm._v(" "),
              _c("li", [
                _c(
                  "a",
                  { attrs: { href: "#popular-posts", "data-toggle": "tab" } },
                  [
                    _c("span", { staticClass: "glyphicon  glyphicon-flash" }),
                    _vm._v("  Popular Posts ")
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "tags  widget-tags" }, [
            _c("h6", [_vm._v("Tags")]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c("a", { staticClass: "tags__link", attrs: { href: "#" } }, [
              _vm._v("Tech")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "tags__link", attrs: { href: "#" } }, [
              _vm._v("Web")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "tags__link", attrs: { href: "#" } }, [
              _vm._v("UI/UX")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "tags__link", attrs: { href: "#" } }, [
              _vm._v("Tutorials")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "tags__link", attrs: { href: "#" } }, [
              _vm._v("Workflow")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "widget-recent-comments  push-down-30" }, [
            _c("h6", [_vm._v("Recent Comments")]),
            _vm._v(" "),
            _c("ul", [
              _c("li", [
                _vm._v(
                  "\n                                    Mihael on\n                                    "
                ),
                _c("a", { attrs: { href: "single-post.html#comments" } }, [
                  _vm._v("ProteusThemes and its products are great")
                ]),
                _vm._v(" "),
                _c("br")
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v(
                  "\n                                    Jaka on\n                                    "
                ),
                _c("a", { attrs: { href: "single-post.html#comments" } }, [
                  _vm._v("Readable is most readable WordPress theme out there")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v(
                  "\n                                    Mihael on\n                                    "
                ),
                _c("a", { attrs: { href: "single-post.html#comments" } }, [
                  _vm._v("Everybody loves Readable")
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "social-icons  widget-social-icons  push-down-30" },
            [
              _c("h6", [_vm._v("Social Icons")]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-facebook" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-twitter" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-skype" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-youtube" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-reddit" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-wordpress" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-bitbucket" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-github" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-html5" })]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "social-icons__container",
                  attrs: { href: "#" }
                },
                [_c("span", { staticClass: "zocial-meetup" })]
              )
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-507dda0f", module.exports)
  }
}

/***/ })

/******/ });