new Vue({
    el: '.storage-list',
    data: {
        message: 'Hello Vue.js!'
    },
    methods: {
        removeStorage: function (id) {
            this.$http.delete(
                'http://ti.loc/storage/' + id, {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }
            ).then(response => {
                if (response.data.remove === true) {
                    let storageId = 'storage-' + response.data.id;
                    let storage = document.getElementById(storageId);

                    storage.remove();
                }
            });
        },
        editStorage: function () {
            console.log('YESSS!!!');
            let div = document.getElementById('storage-body');
            let divContent = div.innerHTML;

            let toolbarOptionsForBody = ['bold', 'italic', 'underline', 'strike', 'code-block'];
            let toolbarOptionsForTitle = ['bold', 'italic', 'underline', 'strike'];

            hljs.configure({
                languages: ['javascript', 'ruby', 'python', 'php', 'html', 'css', 'scss']
            });

            let printTitle = document.createElement("div");
            let storageTitle = document.getElementById('#storage-title');
            printTitle.innerHTML = "Title";

            const quillForTitle = new Quill('#storage-title', {
                theme: 'snow',
                modules: {
                    toolbar: toolbarOptionsForTitle
                }
            });

            const quill = new Quill('#storage-body', {
                theme: 'snow',
                modules: {
                    syntax: true,
                    toolbar: toolbarOptionsForBody
                }
            });
        },
        submitData: function() {
            let bodyObject = document.querySelector('#storage-body');
            let bodyHtmlContent = bodyObject.children[0].innerHTML;

            let titleObject = document.querySelector('#storage-title');
            let titleHtmlContent = titleObject.children[0].innerHTML;

            let tags = document.getElementById('storage-tag').value;

            if (tags.trim() !== '') {
                let tag = new Tags(tags);
                tag = tag.tagsParser();
            } else {
                tag = null;
            }

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: pathStoreStorage,
                data:{
                    body : bodyHtmlContent,
                    title : titleHtmlContent,
                    tag : tag
                },
                success: function(url) {
                    window.location.replace(url);
                },
                error: function (data) {
                    let errors = $.parseJSON(data.responseText);

                    $.each(errors, function (key, value) {
                        $('#' + key).parent().addClass('error');
                    });
                }
            })
        }
    }
});