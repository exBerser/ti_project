'use strict';

let toolbarOptionsForBody = ['bold', 'italic', 'underline', 'strike', 'code-block'];
let toolbarOptionsForTitle = ['bold', 'italic', 'underline', 'strike'];

hljs.configure({
    languages: ['javascript', 'ruby', 'python', 'php', 'html', 'css', 'scss']
});



const quillForTitle = new Quill('#storage-title', {
    theme: 'snow',
    modules: {
        toolbar: toolbarOptionsForTitle
    }
});

const quill = new Quill('#storage-body', {
    theme: 'snow',
    modules: {
        syntax: true,
        toolbar: toolbarOptionsForBody
    }
});

function submitData() {
    let bodyObject = document.querySelector('#storage-body');
    let bodyHtmlContent = bodyObject.children[0].innerHTML;

    let titleObject = document.querySelector('#storage-title');
    let titleHtmlContent = titleObject.children[0].innerHTML;

    let tags = document.getElementById('storage-tag').value;

    let tag = new Tags(tags);

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "POST",
        url: pathStoreStorage,
        data:{
            body : bodyHtmlContent,
            title : titleHtmlContent,
            tag : tag.tagsParser()
        },
        success: function(url) {
            window.location.replace(url);
        },
        error: function (data) {
            let errors = $.parseJSON(data.responseText);

            $.each(errors, function (key, value) {
                $('#' + key).parent().addClass('error');
            });
        }
    })
}