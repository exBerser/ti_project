class Tags {
    constructor(stringTags) {
        this.stringTags = stringTags;
    }

    tagsParser() {
        let tags = this.stringTags.split(',').map(function(tag) {
            return {name:tag.trim()};
        }).filter(function (tag) {
            return tag.name != '';
        });

        this.arrTags = tags;

        return tags;
    }

    ifTagExist($tags) {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: pathStoreStorage,
            data:{
                body : bodyHtmlContent,
                title : titleHtmlContent,
                tag : tag.tagsParser()
            },
            success: function(data) {
                alert('Storage created');
            },
            error: function (data) {
                let errors = $.parseJSON(data.responseText);

                $.each(errors, function (key, value) {
                    $('#' + key).parent().addClass('error');
                });
            }
        })
    }
}