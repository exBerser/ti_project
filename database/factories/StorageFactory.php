<?php

use Faker\Generator as Faker;

$factory->define(App\Storage::class, function (Faker $faker) {
    return [
        'title'             => $faker->sentence,
        'body'              => $faker->paragraph,
        'user_id'           => 78,
    ];
});
