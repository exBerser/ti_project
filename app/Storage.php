<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Storage extends Model
{
    protected $fillable = ['title', 'body', 'user_id'];

    public static function getAll() {
        return Storage::with('user')->get();
    }

    public function add($request)
    {
        $post = Storage::create([
            'title'     => $request->title,
            'body'      => $request->body,
            'user_id'   => $request->userId
        ]);

//        if ($request->tag !== null) {
//            $tags = $this->createTagIfNotExist($request->tag);
//            $this->setTagRelation($tags, $post);
//        }

        return $post;
    }

    public function updateStorage(Request $request, $id) {
        $crud = $this->findOrFail($id);
        $crud->title = $request->title;
        $crud->body = $request->body;
        $crud->save();

        return $crud->save();
    }

    public static function allByFilter($filter)
    {
        return Storage::with('tags')
            ->select('storages.*')
            ->distinct()
            ->join('storage_tag', 'storages.id', '=', 'storage_tag.storage_id')
            ->join('tags', 'tags.id', '=', 'storage_tag.tag_id')
            ->where('tags.name', $filter)->get();
    }

    /**
     * Attach tags to storage
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTagIfNotExist(array $tags): array
    {
        foreach ($tags as &$tag) {
            $tag['id'] = Tag::firstOrCreate($tag)->id;
        }

        return $tags;
    }

    /**
     * Attach tags to storage
     *
     * @param array $tags
     * @param Storage $post
     *
     * @return void
     */
    public function setTagRelation(array $tags, Storage $post): void
    {
        foreach ($tags as $tag) {
            $post->tags()->attach($tag['id']);
        }
    }

    /**
     * Get tags of the storage
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    /**
     * Get the owner of the storage
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}