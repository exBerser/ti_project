<?php

namespace App\Http\Controllers;

use App\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class StorageController extends Controller
{
    /**
     * Get all storages
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return response(Storage::getAll(), Response::HTTP_OK);

        //TODO all by filter
    }

    /**
     * Show storage view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSingle()
    {
        return view('storage.single');
    }

    /**
     * Get storage
     *
     * @param Storage $storage
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function show(Storage $storage)
    {
        return response($storage->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show create view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('storage.create');
    }

    /**
     * Save storage
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'title' => 'required|max:150',
                'body'  => 'required|min:5'
            ]
        );

        $storage = new Storage();
        $storage = $storage->add($request);
        $storage->redirect = route('storages');

        return response($storage->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Update storage
     *
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id) {
        $storage = new Storage();
        $storage->updateStorage($request, $id);

        return response($request, Response::HTTP_OK);
    }

    /**
     * Delete storage
     *
     * @param $id
     *
     * @return array
     */
    public function destroy($id)
    {
        $storage = Storage::find($id);

        return [
            'id'     => $id,
            'remove' => $storage === null ? false : $storage->delete()
        ];

//        TODO to delete multiple rows ($model -> destroy ([1,2,3]);)
    }
}
