let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js/app.js')
    .js('resources/assets/js/storage/create.js', 'public/js/storage/create.js')
    .js('resources/assets/js/storage/list.js', 'public/js/storage/list.js')
    .js('resources/assets/js/storage/single.js', 'public/js/storage/single.js')
    .scripts(
        [
            'node_modules/quill/dist/quill.js'
        ],
        'public/js/quill.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .styles(
        [
            'resources/assets/css/demo.css'
        ],
        'public/css/demo.css'
);